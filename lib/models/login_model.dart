class LoginModel {
  final String? accessToken;
  final String? message;

  LoginModel({this.accessToken, this.message});

  factory LoginModel.fromJson(Map<String, dynamic> json) {
    return LoginModel(
      accessToken: json['access_token'] as String,
      message: json['message'] as String?,
    );
  }
}
