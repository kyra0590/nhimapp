class AnimalsModel {
  final int id;
  final String name;
  final String alpha;
  final String width_150;
  final String width_300;
  final String width_500;

  AnimalsModel({
    required this.id,
    required this.name,
    required this.alpha,
    this.width_150 = '',
    this.width_300 = '',
    this.width_500 = '',
  });

  factory AnimalsModel.fromJson(Map<String, dynamic> json) {
    return AnimalsModel(
      id: json['id'] as int,
      name: json['name'] as String,
      alpha: json['alpha'] as String,
      width_150: json['width_150'] as String,
      width_300: json['width_300'] as String,
      width_500: json['width_500'] as String,
    );
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "alpha": alpha,
        "width_150": width_150,
        "width_300": width_300,
        "width_500": width_500,
      };
}
