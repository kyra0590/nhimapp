import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants/preferences.dart';

class SharedPreferenceHelper {
  static SharedPreferenceHelper? _instance;

  static SharedPreferences? _preferences;

  static Future<SharedPreferenceHelper> getInstance() async {
    if (_instance == null) {
      _instance = SharedPreferenceHelper();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance!;
  }

  // General Methods: ----------------------------------------------------------
  Future<String?> get authToken async {
    return _preferences?.getString(Preferences.auth_token);
  }

  Future<bool> saveAuthToken(String authToken) async {
    return _preferences!.setString(Preferences.auth_token, authToken);
  }

  Future<bool> removeAuthToken() async {
    return _preferences!.remove(Preferences.auth_token);
  }

  // Login:---------------------------------------------------------------------
  Future<bool> get isLoggedIn async {
    return _preferences!.getBool(Preferences.is_logged_in) ?? false;
  }

  Future<bool> saveIsLoggedIn(bool value) async {
    return _preferences!.setBool(Preferences.is_logged_in, value);
  }
  // isLocal

  // Login:---------------------------------------------------------------------
  Future<bool> get isLocal async {
    return _preferences!.getBool(Preferences.is_logged_in) ?? false;
  }

  Future<bool> saveIsLocal(bool value) async {
    return _preferences!.setBool(Preferences.is_logged_in, value);
  }

  // optional for custom key
  static Future setString(String key, String value) async =>
      await _preferences!.setString(key, value);

  static String? getString(String key) => _preferences!.getString(key);

  static int? getInt(String key) => _preferences!.getInt(key);

  static bool? getBool(String key) => _preferences!.getBool(key);

  static double? getDouble(String key) => _preferences!.getDouble(key);

  static List<String>? getStringList(String key) =>
      _preferences!.getStringList(key);
}
