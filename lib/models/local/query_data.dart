import 'package:nhimlearning_app/services/local_database.dart';
import 'package:sqflite/sqlite_api.dart';

LocalDatabase localDatabase = LocalDatabase.instance;

class QueryData {
  static Database db = localDatabase.database as Database;

  Future<List> getAllAnimalsLocal(String alpha) async {
    String sql = "SELECT * FROM animals WHERE alpha = '$alpha'";
    var result = await localDatabase.getAllData(sql);
    return result;
  }

  Future<List> getAllAnimalsCartoon(String name) async {
    String sql =
        "SELECT * FROM animals_cartoon WHERE name_en like '%$name%' or name_vn like '%$name%' ";
    var result = await localDatabase.getAllData(sql);
    return result;
  }

  Future<List> getRandomData(
      {String table = "", int limit = 5, String except = ""}) async {
    String sql = "SELECT * FROM $table ";
    if (except != "") {
      sql += " WHERE id NOT IN ($except)";
    }
    sql += " ORDER BY RANDOM() LIMIT $limit";
    var result = await localDatabase.getAllData(sql);
    return result;
  }

  Future<dynamic> getTotalData({table}) async {
    String sql = "SELECT COUNT(*) as total FROM $table";
    dynamic result = await localDatabase.getTotalData(sql);
    return result;
  }

  Future<List> getAllData(
      {String table = "", int limit = 5, String except = ""}) async {
    String sql = "SELECT * FROM $table ";
    if (except != "") {
      sql += " WHERE id NOT IN ($except)";
    }
    if (limit > 0) {
      sql += " LIMIT $limit";
    }
    var result = await localDatabase.getAllData(sql);
    return result;
  }
}
