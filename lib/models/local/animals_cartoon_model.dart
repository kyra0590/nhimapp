class AnimalsCartoonModel {
  final int id;
  final String nameEn;
  final String nameVn;
  final String link;

  AnimalsCartoonModel({
    required this.id,
    required this.nameEn,
    this.nameVn = '',
    required this.link,
  });

  factory AnimalsCartoonModel.fromJson(Map<String, dynamic> json) {
    return AnimalsCartoonModel(
      id: json['id'] as int,
      nameEn: json['name_en'] as String,
      nameVn: json['name_vn'] as String,
      link: json['link'] as String,
    );
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "nameEn": nameEn,
        "nameVn": nameVn,
        "link": link,
      };
}
