final String tableUsers = 'users';

class UserFields {
  static final String id = '_id';
  static final String userName = 'user_name';
  static final String accessToken = 'access_token';
  static final String fullName = 'full_name';
  static final String email = 'email';
}

class UserLocal {
  final int? id;
  final String userName;
  final String accessToken;
  final String fullName;
  final String email;

  const UserLocal(
      {this.id,
      required this.userName,
      required this.accessToken,
      required this.fullName,
      required this.email});
}
