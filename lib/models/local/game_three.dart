class GameThreeModel {
  final int id;
  final String name;

  GameThreeModel({
    required this.id,
    required this.name,
  });

  factory GameThreeModel.fromJson(Map<String, dynamic> json) {
    return GameThreeModel(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
      };
}
