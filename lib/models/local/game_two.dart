class GameTwoModel {
  final int id;
  final String name;

  GameTwoModel({
    required this.id,
    required this.name,
  });

  factory GameTwoModel.fromJson(Map<String, dynamic> json) {
    return GameTwoModel(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
      };
}
