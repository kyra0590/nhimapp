import 'package:nhimlearning_app/models/local/game_two.dart';
import 'package:nhimlearning_app/models/local/query_data.dart';

class GameTwoViewModel {
  QueryData queryData = QueryData();

  Future<dynamic> getRandomIcons(limit, except) async {
    List<dynamic> list = [];
    List data = await queryData.getRandomData(
        table: "gametwo", limit: limit, except: except);
    if (data.length > 0) {
      for (var item in data) {
        GameTwoModel icon = GameTwoModel.fromJson(item);
        list.add(icon);
      }
    }
    return list;
  }

  Future<dynamic> getTotalIcons() async {
    dynamic data = await queryData.getTotalData(table: "gametwo");
    return data;
  }
}
