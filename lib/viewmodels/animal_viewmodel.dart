import 'package:nhimlearning_app/constants/status.dart';
import 'package:nhimlearning_app/models/sharedpref/constants/preferences.dart';
import 'package:nhimlearning_app/services/api_response.dart';
import 'package:nhimlearning_app/services/web_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AnimalsViewModel {
  ApiResponse apiResponse = ApiResponse();

  ApiResponse get response {
    return apiResponse;
  }

  Future<dynamic> getAnimalsByAlphabet(String alpha) async {
    apiResponse.loading();
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String? token = preferences.getString(Preferences.auth_token);
      dynamic queryParameters = {'alpha': alpha, 'noLimit': 1};
      ApiResponse result =
          await WebService().getResponse('/animals', queryParameters, token!);
      apiResponse.completed(result.data, result.statusCode);
    } catch (e) {
      apiResponse.error(e.toString(), Status.codeServerError);
    }
    return apiResponse;
  }
}
