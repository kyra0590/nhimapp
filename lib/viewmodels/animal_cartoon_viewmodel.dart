import 'package:nhimlearning_app/models/local/animals_cartoon_model.dart';
import 'package:nhimlearning_app/models/local/query_data.dart';

class AnimalCartoonViewModel {
  QueryData queryData = QueryData();

  Future<dynamic> getAnimalCartoonByName(name) async {
    List<dynamic> list = [];
    List data = await queryData.getAllAnimalsCartoon(name);
    if (data.isNotEmpty) {
      for (var item in data) {
        AnimalsCartoonModel animal = AnimalsCartoonModel.fromJson(item);
        list.add(animal);
      }
    }
    return list;
  }

  Future<dynamic> getRandomAnimals(limit, except) async {
    List<dynamic> list = [];
    List data = await queryData.getRandomData(
        table: "animals_cartoon", limit: limit, except: except);
    if (data.length > 0) {
      for (var item in data) {
        AnimalsCartoonModel animal = AnimalsCartoonModel.fromJson(item);
        list.add(animal);
      }
    }
    return list;
  }

  Future<dynamic> getTotalAnimals() async {
    dynamic data = await queryData.getTotalData();
    return data;
  }
}
