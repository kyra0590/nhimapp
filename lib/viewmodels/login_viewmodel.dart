import 'package:dio/dio.dart';
import 'package:nhimlearning_app/constants/status.dart';
import 'package:nhimlearning_app/services/api_response.dart';
import 'package:nhimlearning_app/services/web_service.dart';

class LoginViewModel {
  ApiResponse apiResponse = ApiResponse();

  Future<dynamic> fetchLogin(String email, String password) async {
    apiResponse.loading();
    try {
      final formData = FormData.fromMap({
        'email': email,
        'password': password,
      });
      apiResponse = await WebService().postResponse('/login', formData, '');
    } catch (e) {
      apiResponse.error(e.toString(), Status.codeServerError);
    }
    return apiResponse;
  }
}
