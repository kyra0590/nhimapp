import 'package:nhimlearning_app/constants/status.dart';
import 'package:nhimlearning_app/models/sharedpref/constants/preferences.dart';
import 'package:nhimlearning_app/services/api_response.dart';
import 'package:nhimlearning_app/services/web_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TextBooksViewModel {
  ApiResponse apiResponse = ApiResponse();

  ApiResponse get response {
    return apiResponse;
  }

  Future<dynamic> getTextBooksByName(String title) async {
    apiResponse.loading();
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String? token = preferences.getString(Preferences.auth_token);
      dynamic queryParameters = {'title': title, 'noLimit': 1};
      ApiResponse result =
          await WebService().getResponse('/textbooks', queryParameters, token!);
      apiResponse.completed(result.data, result.statusCode);
    } catch (e) {
      apiResponse.error(e.toString(), Status.codeServerError);
    }

    print(apiResponse);
    return apiResponse;
  }
}
