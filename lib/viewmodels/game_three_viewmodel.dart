import 'package:nhimlearning_app/models/local/game_three.dart';
import 'package:nhimlearning_app/models/local/query_data.dart';

class GameThreeViewModel {
  QueryData queryData = QueryData();

  Future<dynamic> getAllImages(limit, except) async {
    List<dynamic> list = [];
    List data = await queryData.getAllData(
        table: "gamethree", limit: limit, except: except);
    if (data.length > 0) {
      for (var item in data) {
        GameThreeModel image = GameThreeModel.fromJson(item);
        list.add(image);
      }
    }
    return list;
  }
}
