import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/models/animals_model.dart';
import 'package:nhimlearning_app/models/local/query_data.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';
import 'package:nhimlearning_app/utils/utils.dart';
import 'package:nhimlearning_app/view/widgets/categories_rows.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:nhimlearning_app/viewmodels/animal_viewmodel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

List<String> alphas = CStrings.rangeAlphabets;

class AnimalsLocalPage extends StatefulWidget {
  const AnimalsLocalPage({Key? key}) : super(key: key);

  @override
  _AnimalsLocalPageState createState() => _AnimalsLocalPageState();
}

class _AnimalsLocalPageState extends State<AnimalsLocalPage> {
  TextToSpeech tts = TextToSpeech();
  AnimalsViewModel viewmodel = AnimalsViewModel();
  int? indexAlpha;
  List<AnimalsModel>? items;
  Utils utils = Utils();
  QueryData queryData = QueryData();

  @override
  void initState() {
    indexAlpha = 0;
    loadAnimals();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    loadAnimals();
    super.didChangeDependencies();
  }

  void loadAnimals() async {
    items = await loadItemsByAlphabel(indexAlpha);
    if (mounted) setState(() {});
  }

  @override
  void didUpdateWidget(covariant AnimalsLocalPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  onChangeCallBack(value) {
    EasyLoading.show(status: "Loading ....");
    setState(() {
      indexAlpha = value;
    });
    loadAnimals();
    EasyLoading.dismiss();
  }

  Future<List<AnimalsModel>> loadItemsByAlphabel(value) async {
    List<AnimalsModel> newAnimals = [];
    String alpha = alphas[value].toLowerCase();
    List data = await queryData.getAllAnimalsLocal(alpha);
    if (data.isNotEmpty) {
      for (var item in data) {
        AnimalsModel animal = AnimalsModel.fromJson(item);
        newAnimals.add(animal);
      }
    }
    return newAnimals;
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          lang.headerAnimals,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: CategoriesRows(
              indexAlpha: indexAlpha,
              callback: (value) {
                onChangeCallBack(value);
              },
            ),
          ),
          Flexible(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Container(
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 300,
                    childAspectRatio: 0.7,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: items != null ? items!.length : 0,
                  itemBuilder: (BuildContext ctx, index) {
                    return GestureDetector(
                      onTap: () => {
                        tts.text = items![index].name.toLowerCase(),
                        tts.speak()
                      },
                      child: Card(
                        color: Colors.redAccent,
                        child: Column(
                          children: [
                            Flexible(
                              flex: 8,
                              child: Image.network(
                                items![index].width_300,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                items![index].name,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.center,
                                softWrap: true,
                              ),
                            ),
                            // Container(
                            //   margin: EdgeInsets.only(top: 10),
                            //   child: Text(
                            //     "",
                            //     // "'" + items![index].name + "'",
                            //     style: TextStyle(
                            //       fontSize: 15,
                            //       color: Colors.yellow,
                            //     ),
                            //     textAlign: TextAlign.center,
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
      drawer: DrawerMenu(),
    );
  }
}
