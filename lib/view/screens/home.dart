import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/menus.dart';
import 'package:nhimlearning_app/models/sharedpref/shared_preference_helper.dart';
import 'package:nhimlearning_app/services/service_locator.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nhimlearning_app/view/widgets/item_dashboard.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List? listItems = [];
  SharedPreferenceHelper _sharedPrefsHelper = locator<SharedPreferenceHelper>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    loadMenu();
    if (mounted) setState(() {});
    super.didChangeDependencies();
  }

  loadMenu() async {
    bool isLocal = await _sharedPrefsHelper.isLocal;
    var lang = AppLocalizations.of(context);
    Menus menus = Menus(lang: lang);

    if (isLocal == true) {
      listItems = menus.loadMenuLocal();
    } else {
      listItems = menus.loadMenuOnline();
    }

    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          lang.headerHome,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      drawer: DrawerMenu(),
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Container(
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 1.0,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemCount: listItems!.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return ItemDashboard(
                      listItems![index]['icon'],
                      listItems![index]['title'],
                      '',
                      Colors.lightBlue,
                      Colors.redAccent,
                      () => {
                        Navigator.of(context)
                            .popAndPushNamed(listItems![index]['link'])
                      },
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
