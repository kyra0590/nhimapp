import 'dart:async';
import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/provider/locale_provider.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:nhimlearning_app/view/widgets/fade_animation.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nhimlearning_app/models/sharedpref/shared_preference_helper.dart';
import 'package:nhimlearning_app/services/service_locator.dart';


class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final SharedPreferenceHelper _sharedPrefsHelper =
      locator<SharedPreferenceHelper>();

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);

    return Material(
      child: FadeAnimation(
        1.6,
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage(Assets.bgLeaf),
            fit: BoxFit.cover,
          )),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    Assets.appLogo,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    lang.appName.toUpperCase(),
                    style: TextStyle(
                      color: Colors.red[400],
                      fontSize: 37,
                      fontWeight: FontWeight.bold,
                      shadows: [
                        Shadow(
                          offset: Offset(2.0, 2.0),
                          blurRadius: 5.0,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ), // AppIconWidget(image: Assets.appLogo)),
    );
  }

  startTimer() {
    var _duration = Duration(milliseconds: 2000);
    return Timer(_duration, navigate);
  }

  navigate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String lang = preferences.getString(CStrings.language) ?? 'en';
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    provider.setLocale(Locale(lang));
    // bool? isLogged = preferences.getBool(Preferences.is_logged_in);
    await _sharedPrefsHelper.saveAuthToken("");
        await _sharedPrefsHelper.saveIsLocal(true);
        await _sharedPrefsHelper.saveIsLoggedIn(true);
    // if (isLogged == true) {
      Navigator.of(context).pushReplacementNamed(Routes.home);
    // } else {
      // Navigator.of(context).pushReplacementNamed(Routes.login);
    // }
  }
}
