import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/constants/color_layout.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/services/api_response.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';
import 'package:nhimlearning_app/view/widgets/card_textbook.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:nhimlearning_app/view/widgets/search_bar.dart';
import 'package:nhimlearning_app/viewmodels/textbooks_viewmodel.dart';

class TextBooksPage extends StatefulWidget {
  @override
  State<TextBooksPage> createState() => _TextBooksPageState();
}

class _TextBooksPageState extends State<TextBooksPage> {
  final TextBooksViewModel viewModel = TextBooksViewModel();
  late List<dynamic> listItem = [];
  String name = '';
  bool init = false;
  TextToSpeech tts = TextToSpeech();

  @override
  void didChangeDependencies() {
    loadData();
    super.didChangeDependencies();
  }

  loadData() async {
    if (!init) {
      this.name = '';
      init = true;
    }
    listItem = [];
    ApiResponse res = await viewModel.getTextBooksByName(this.name);
    listItem = res.data;

    if (mounted) setState(() {});
  }

  onChangeName(value) {
    setState(() {
      name = value;
    });
    loadData();
  }

  speakText() async {
    tts.text = this.name;
    tts.speak();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: ColorLayout.bgAnimalCartoon,
      drawer: DrawerMenu(),
      body: Builder(
        builder: (BuildContext context) {
          return Stack(
            children: [
              Container(
                height: size.height * .35,
                decoration: BoxDecoration(
                  color: Colors.red[100],
                  image: DecorationImage(
                    alignment: Alignment.topCenter,
                    image: AssetImage(Assets.logoNoColor),
                  ),
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              height: 52,
                              width: 52,
                              decoration: BoxDecoration(
                                color: Colors.red[200],
                                shape: BoxShape.circle,
                              ),
                              child: IconButton(
                                onPressed: () =>
                                    Scaffold.of(context).openDrawer(),
                                icon: Icon(
                                  Icons.menu,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Text(
                                CStrings.headerTextBooks,
                                style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 30,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              height: 52,
                              width: 52,
                              decoration: BoxDecoration(
                                color: Colors.red[200],
                                shape: BoxShape.circle,
                              ),
                              child: IconButton(
                                onPressed: () => {
                                  Navigator.of(context)
                                      .popAndPushNamed(Routes.home),
                                },
                                icon: Icon(
                                  Icons.home,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SearchBar(
                        name: this.name,
                        onChange: this.onChangeName,
                      ),
                      Expanded(
                        flex: 1,
                        child: GridView.builder(
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 200,
                              childAspectRatio: 0.85,
                              crossAxisSpacing: 20,
                              mainAxisSpacing: 20,
                            ),
                            itemCount: listItem.length,
                            itemBuilder: (BuildContext ctx, index) {
                              return CardTextBook(
                                title: listItem[index]['title'],
                                imageLink: CStrings.apiImageUrl +
                                    listItem[index]['image'],
                                isTextToSpeech: true,
                                width: 150,
                                height: 180,
                              );
                            }),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
