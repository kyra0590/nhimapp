import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/provider/locale_provider.dart';
import 'package:nhimlearning_app/store/login/login_mbx.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:nhimlearning_app/view/widgets/fade_animation.dart';
import 'package:nhimlearning_app/view/widgets/toast_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginMbx loginMbx = LoginMbx();
  ToastWidget toast = ToastWidget();

  @override
  void initState() {
    super.initState();
    loginMbx.setupValidations();
  }

  @override
  void dispose() {
    loginMbx.dispose();
    super.dispose();
  }

  void loginUser() async {
    var lang = await AppLocalizations.delegate.load(Locale(loginMbx.lang));
    EasyLoading.show(status: lang.loading);
    await loginMbx.executeLogin();
    if (loginMbx.canLogin) {
      toast.successMessage(lang.messageLoginSuccess);
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.setString(CStrings.language, loginMbx.lang);
      Navigator.of(context).pushReplacementNamed(Routes.home);
    } else {
      toast.errorMessage(lang.messageLoginFailed);
    }
    EasyLoading.dismiss();
  }

  onChangeLang(value) {
    loginMbx.setLang(value);
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    provider.setLocale(Locale(value));
    loginUser();
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                height: 350,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(Assets.loginBackground),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      left: 30,
                      width: 80,
                      height: 200,
                      child: FadeAnimation(
                        1,
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                Assets.loginLight_1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 140,
                      width: 80,
                      height: 150,
                      child: FadeAnimation(
                        1.3,
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                Assets.loginLight_2,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 40,
                      top: 40,
                      width: 80,
                      height: 150,
                      child: FadeAnimation(
                        1.5,
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                Assets.loginClock,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      child: FadeAnimation(
                        1.6,
                        Container(
                          margin: EdgeInsets.only(top: 50),
                          child: Center(
                            child: Text(
                              lang.headerLogin,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 40,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  children: <Widget>[
                    FadeAnimation(
                      1.8,
                      Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(143, 148, 251, .2),
                              blurRadius: 20.0,
                              offset: Offset(0, 10),
                            ),
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Colors.grey.shade100,
                                  ),
                                ),
                              ),
                              child: Observer(
                                builder: (_) => TextField(
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Email",
                                    hintStyle: TextStyle(
                                      color: Colors.grey.shade400,
                                    ),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                    errorText: loginMbx.error.email,
                                  ),
                                  onChanged: (value) => loginMbx.email = value,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: Observer(
                                builder: (_) => TextField(
                                  decoration: InputDecoration(
                                    // labelText: loginMbx.password,
                                    border: InputBorder.none,
                                    hintText: lang.password,
                                    hintStyle: TextStyle(
                                      color: Colors.grey.shade400,
                                    ),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                    errorText: loginMbx.error.password,
                                  ),
                                  onChanged: (value) =>
                                      loginMbx.password = value,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.language,
                            ),
                          ),
                          Container(
                            child: DropdownButton<String>(
                              value: lang.localeName,
                              iconSize: 24,
                              onChanged: onChangeLang,
                              items: [
                                for (var item in CStrings.languages)
                                  DropdownMenuItem<String>(
                                    value: item,
                                    child: Text(
                                      item,
                                      style: TextStyle(
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    FadeAnimation(
                      2,
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(143, 148, 251, 1),
                              Color.fromRGBO(143, 148, 251, .6),
                            ],
                          ),
                        ),
                        child: Container(
                          width: double.infinity,
                          child: TextButton(
                            onPressed: loginUser,
                            child: Text(
                              lang.login,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 70,
                    ),
                    // FadeAnimation(1.5, Text("Forgot Password?", style: TextStyle(color: Color.fromRGBO(143, 148, 251, 1)),)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
