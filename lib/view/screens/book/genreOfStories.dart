import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';

class GenreOfStories extends StatefulWidget {
  @override
  StateGenreOfStories createState() => StateGenreOfStories();
}

class StateGenreOfStories extends State<GenreOfStories> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          lang.allegories,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      drawer: DrawerMenu(),
      body: Text('asdasd'),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: lang.grimmStories,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: lang.andersenStories,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: lang.arabianNightsStories,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (index) => {
          setState(() {
            _selectedIndex = index;
          }),
        },
      ),
    );
  }
}
