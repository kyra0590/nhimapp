import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/view/screens/languages.dart';
import 'package:nhimlearning_app/view/widgets/setting_ui/custom_section.dart';
import 'package:nhimlearning_app/view/widgets/setting_ui/setting_tiles.dart';
import 'package:nhimlearning_app/view/widgets/settings_ui.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool lockInBackground = true;
  bool notificationsEnabled = true;

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(lang.headerSettings),
      ),
      backgroundColor: Colors.grey[300],
      body: SettingsList(
        shrinkWrap: true,
        sections: [
          SettingsSection(
            titlePadding: EdgeInsets.all(10),
            title: lang.common,
            tiles: [
              SettingsTile(
                title: lang.language,
                subtitle: lang.currentLang,
                leading: Icon(Icons.language),
                onPressed: (context) {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => LanguagesPage(),
                  ));
                },
              ),
              // CustomTile(
              //   child: Container(
              //     color: Color(0xFFEFEFF4),
              //     padding: EdgeInsetsDirectional.only(
              //       start: 14,
              //       top: 12,
              //       bottom: 30,
              //       end: 14,
              //     ),
              //   ),
              // ),
              // SettingsTile(
              //   title: 'Environment',
              //   subtitle: 'Production',
              //   leading: Icon(Icons.cloud_queue),
              // ),
            ],
          ),
          // SettingsSection(
          //   title: 'Account',
          //   tiles: [
          //     SettingsTile(title: 'Phone number', leading: Icon(Icons.phone)),
          //     SettingsTile(title: 'Email', leading: Icon(Icons.email)),
          //     SettingsTile(title: 'Sign out', leading: Icon(Icons.exit_to_app)),
          //   ],
          // ),
          // SettingsSection(
          //   title: 'Security',
          //   tiles: [
          //     SettingsTile.switchTile(
          //       title: 'Lock app in background',
          //       leading: Icon(Icons.phonelink_lock),
          //       switchValue: lockInBackground,
          //       onToggle: (bool value) {
          //         setState(() {
          //           lockInBackground = value;
          //           notificationsEnabled = value;
          //         });
          //       },
          //     ),
          //     SettingsTile.switchTile(
          //       title: 'Use fingerprint',
          //       subtitle: 'Allow application to access stored fingerprint IDs.',
          //       leading: Icon(Icons.fingerprint),
          //       onToggle: (bool value) {},
          //       switchValue: false,
          //     ),
          //     SettingsTile.switchTile(
          //       title: 'Change password',
          //       leading: Icon(Icons.lock),
          //       switchValue: true,
          //       onToggle: (bool value) {},
          //     ),
          //     SettingsTile.switchTile(
          //       title: 'Enable Notifications',
          //       enabled: notificationsEnabled,
          //       leading: Icon(Icons.notifications_active),
          //       switchValue: true,
          //       onToggle: (value) {},
          //     ),
          //   ],
          // ),
          // SettingsSection(
          //   title: 'Misc',
          //   tiles: [
          //     SettingsTile(
          //         title: 'Terms of Service', leading: Icon(Icons.description)),
          //     SettingsTile(
          //         title: 'Open source licenses',
          //         leading: Icon(Icons.collections_bookmark)),
          //   ],
          // ),
          CustomSection(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 22, bottom: 8),
                  child: Image.asset(
                    Assets.appLogo,
                    height: 50,
                    width: 50,
                    // color: Color(0xFF777777),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 22),
                  child: Text(
                    'Version: 1.0.1 (100)',
                    style: TextStyle(
                      color: Color(0xFF777777),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      // TileSettings();
    );
  }
}
