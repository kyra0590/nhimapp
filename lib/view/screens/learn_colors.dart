import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

List<dynamic> listColors = CStrings.listColors;

class LearnColors extends StatefulWidget {
  @override
  _LearnColorsState createState() => _LearnColorsState();
}

class _LearnColorsState extends State<LearnColors> {
  TextToSpeech tts = TextToSpeech();

  @override
  void didChangeDependencies() {
    autoLoadSpeak();
    if (mounted) setState(() {});
    super.didChangeDependencies();
  }

  autoLoadSpeak() async {
    if (Platform.isAndroid) {
      var lang = AppLocalizations.of(context);
      if (lang.localeName == 'vi') {
        tts.setSpeakVNAndroid();
      } else {
        tts.setSpeakEn();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          lang.colors,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      drawer: DrawerMenu(),
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Container(
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemCount: listColors.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return GestureDetector(
                      onTap: () => {
                        tts.text = lang.localeName == 'en'
                            ? listColors[index][lang.localeName]
                            : "Màu " + listColors[index][lang.localeName],
                        tts.speak(),
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          listColors[index][lang.localeName],
                          style: TextStyle(
                            color: Color(listColors[index]['color']) ==
                                    Colors.black
                                ? Colors.white
                                : Colors.black,
                            fontSize: 23,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(width: 1, color: Colors.black),
                          color: Color(listColors[index]['color']),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
