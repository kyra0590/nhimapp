import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/constants/color_layout.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:nhimlearning_app/view/widgets/card_textbook.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:nhimlearning_app/view/widgets/search_bar.dart';
import 'package:nhimlearning_app/viewmodels/animal_cartoon_viewmodel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AnimalCartoonPage extends StatefulWidget {
  @override
  State<AnimalCartoonPage> createState() => _AnimalCartoonPageState();
}

class _AnimalCartoonPageState extends State<AnimalCartoonPage> {
  final AnimalCartoonViewModel viewModel = AnimalCartoonViewModel();
  late List<dynamic> listItem = [];
  String name = '';
  bool init = false;

  @override
  void didChangeDependencies() {
    loadAnimals();
    super.didChangeDependencies();
  }

  loadAnimals() async {
    if (!init) {
      this.name = '';
      init = true;
    }
    listItem = [];
    listItem = await viewModel.getAnimalCartoonByName(this.name);
    if (mounted) setState(() {});
  }

  onChangeName(value) {
    setState(() {
      name = value;
    });
    loadAnimals();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var lang = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: ColorLayout.bgAnimalCartoon,
      drawer: DrawerMenu(),
      body: Builder(
        builder: (BuildContext context) {
          return Stack(
            children: [
              Container(
                height: size.height * .35,
                decoration: BoxDecoration(
                  color: Colors.red[100],
                  image: DecorationImage(
                    alignment: Alignment.topCenter,
                    image: AssetImage(
                      Assets.logoNoColor,
                    ),
                  ),
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              height: 52,
                              width: 52,
                              decoration: BoxDecoration(
                                color: Colors.red[200],
                                shape: BoxShape.circle,
                              ),
                              child: IconButton(
                                onPressed: () =>
                                    Scaffold.of(context).openDrawer(),
                                icon: Icon(
                                  Icons.menu,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Text(
                                lang.headerAnimals,
                                style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 30,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              height: 52,
                              width: 52,
                              decoration: BoxDecoration(
                                color: Colors.red[200],
                                shape: BoxShape.circle,
                              ),
                              child: IconButton(
                                onPressed: () => {
                                  Navigator.of(context)
                                      .popAndPushNamed(Routes.home),
                                },
                                icon: Icon(
                                  Icons.home,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SearchBar(
                        name: this.name,
                        onChange: this.onChangeName,
                      ),
                      Expanded(
                        flex: 1,
                        child: GridView.builder(
                          shrinkWrap: true,
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200,
                            childAspectRatio: 0.65,
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20,
                          ),
                          itemCount: listItem.length,
                          itemBuilder: (BuildContext ctx, index) {
                            return CardTextBook(
                              title: lang.localeName == 'en'
                                  ? listItem[index].nameEn
                                  : listItem[index].nameVn,
                              desc: lang.localeName == 'vi'
                                  ? listItem[index].nameEn
                                  : listItem[index].nameVn,
                              imageLink: listItem[index].link,
                              isTextToSpeech: true,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
