import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

List<String> alphas = CStrings.rangeAlphabets;

class AlphabetsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AlphabetsPageState();
}

class _AlphabetsPageState extends State<AlphabetsPage> {
  TextToSpeech tts = TextToSpeech();

  @override
  void didChangeDependencies() {
    autoLoadSpeak();
    if (mounted) setState(() {});
    super.didChangeDependencies();
  }

  autoLoadSpeak() async {
    if (Platform.isAndroid) {
      var lang = AppLocalizations.of(context);
      if (lang.localeName == 'vi') {
        tts.setSpeakVNAndroid();
      } else {
        tts.setSpeakEn();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          lang.headerAlphabets,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      drawer: DrawerMenu(),
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Container(
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemCount: alphas.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return GestureDetector(
                      onTap: () => {
                        tts.text = alphas[index].toString().toLowerCase(),
                        tts.speak()
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          alphas[index].toString(),
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
