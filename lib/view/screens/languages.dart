import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/provider/locale_provider.dart';
import 'package:nhimlearning_app/view/widgets/settings_ui.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguagesPage extends StatefulWidget {
  @override
  _LanguagesPageState createState() => _LanguagesPageState();
}

class _LanguagesPageState extends State<LanguagesPage> {
  List<String> languages = CStrings.languages;
  int languageIndex = 0;
  late SharedPreferences preferences;

  @override
  void didChangeDependencies() {
    autoLoadLang();
    if (mounted) setState(() {});
    super.didChangeDependencies();
  }

  Widget trailingWidget(int index) {
    return (languageIndex == index)
        ? Icon(Icons.check, color: Colors.blue)
        : Icon(null);
  }

  void changeLanguage(int index) {
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    provider.setLocale(Locale(languages[index]));
    saveLanguage(languages[index]);
    setState(() {
      languageIndex = index;
    });
  }

  autoLoadLang() async {
    preferences = await SharedPreferences.getInstance();
    String langName = preferences.getString(CStrings.language) ?? 'en';
    setState(() {
      languageIndex = languages.indexOf(langName);
    });
  }

  saveLanguage(value) async {
    await preferences.setString(CStrings.language, value);
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(title: Text(lang.language)),
      body: SettingsList(
        sections: [
          SettingsSection(
            tiles: [
              SettingsTile(
                title: lang.english,
                trailing: trailingWidget(0),
                onPressed: (BuildContext context) {
                  changeLanguage(0);
                },
              ),
              SettingsTile(
                title: lang.vietnam,
                trailing: trailingWidget(1),
                onPressed: (BuildContext context) {
                  changeLanguage(1);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
