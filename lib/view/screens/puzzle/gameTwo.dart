import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:nhimlearning_app/constants/sounds.dart';
import 'package:nhimlearning_app/utils/utils.dart';
import 'package:nhimlearning_app/view/widgets/audio_widget.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:nhimlearning_app/viewmodels/game_two_viewmodel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

AudioWidget audioPlayer = AudioWidget();

class GameTwo extends StatefulWidget {
  const GameTwo({Key? key}) : super(key: key);

  @override
  _StateGameTwo createState() => _StateGameTwo();
}

class _StateGameTwo extends State<GameTwo> {
  final GameTwoViewModel viewModel = GameTwoViewModel();
  late List<dynamic> listItem = [];
  List<int> iconsUsed = [];
  List listIcons = [];
  List listMatch = [];
  int totalIcons = 0;
  bool init = false;
  Utils utils = Utils();
  int limit = 4;
  List<int> listRandomNumber = [];
  int numberRandom = 0;

  @override
  void initState() {
    loadIcons();
    getTotalIcons();
    super.initState();
  }

  getTotalIcons() async {
    List data = await viewModel.getTotalIcons();
    if (data.isNotEmpty) {
      totalIcons = data[0]['total'].toInt();
    }
  }

  loadIcons() async {
    if (!init) {
      init = true;
      listIcons = [];
    }
    await EasyLoading.show(status: "Loading ....");
    String except = iconsUsed.isEmpty ? "" : iconsUsed.join(",").toString();
    listItem = await viewModel.getRandomIcons(limit, except);
    listMatch = [];
    if (mounted) setState(() {});
    await EasyLoading.dismiss();
  }

  addCorrectAnswer(value) async {
    if (value != '' && !listMatch.contains(value)) {
      setState(() {
        listMatch.add(value);
      });
      audioPlayer.playLocal(path: Sounds.correct);
    }

    if (listMatch.length == limit) {
      audioPlayer.playLocal(path: Sounds.crabdance);
      loadDialog();
    }
  }

  loadDialog() {
    var lang = AppLocalizations.of(context);

    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(
              lang.nextLevel,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.teal),
            ),
            content: Container(
              height: 100,
              width: 100,
              child: IconButton(
                icon: Icon(Icons.forward, size: 80, color: Colors.orange),
                onPressed: () => {
                  Navigator.pop(context),
                  loadIcons(),
                  audioPlayer.stopLocal()
                },
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          loadIcons();
        },
        child: Icon(Icons.restart_alt),
        backgroundColor: Colors.green,
        splashColor: Colors.orange,
      ),
      drawer: DrawerMenu(),
      appBar: AppBar(
        title: Text(lang.gameTwo),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Expanded(
              child: Card(
                borderOnForeground: true,
                color: Colors.white,
                elevation: 10,
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 1.2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: listItem.length,
                  itemBuilder: (context, index) {
                    return Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      color: Colors.transparent,
                      child: Draggable(
                        data: listItem[index].name,
                        child: ItemGameTwo(
                            fontSize: 80,
                            text: listMatch.isNotEmpty &&
                                    listMatch.contains(listItem[index].name)
                                ? ''
                                : listItem[index].name),
                        feedback: ItemGameTwo(text: listItem[index].name),
                        childWhenDragging: ItemGameTwo(),
                      ),
                    );
                  },
                ),
              ),
            ),
            Expanded(
              child: Card(
                borderOnForeground: true,
                shadowColor: Colors.black,
                color: Colors.white,
                elevation: 10,
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 1.2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: listItem.length,
                  itemBuilder: (context, index) {
                    return Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      child: DragTarget(
                        builder: (context, accepted, rejected) {
                          return ItemGameTwo(
                            text: listItem[index]!.name,
                            isCorrect: listMatch.isNotEmpty &&
                                listMatch.contains(listItem[index]!.name),
                            fontSize: 80,
                          );
                        },
                        onAccept: (value) {
                          if (listItem[index].name == value) {
                            addCorrectAnswer(value);
                          } else {
                            audioPlayer.playLocal(path: Sounds.wrong);
                          }
                        },
                        onWillAccept: (value) =>
                            (!listMatch.contains(value) && value != ''),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ItemGameTwo extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  final double fontSize;
  final bool isCorrect;
  final double opacity;

  ItemGameTwo({
    this.text = '',
    this.width = 100,
    this.height = 100,
    this.fontSize = 80,
    this.isCorrect = false,
    this.opacity = 0.5,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Stack(
        children: [
          Text(
            this.text,
            style: TextStyle(
              fontSize: fontSize,
            ),
          ),
          if (isCorrect) ...[
            Opacity(
              opacity: opacity,
              child: Container(
                width: width,
                height: height,
                color: Colors.white,
              ),
            ),
            Icon(
              Icons.check,
              size: width,
              color: Colors.green,
            ),
          ],
        ],
      ),
    );
  }
}
