import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';

class GamePaint extends StatefulWidget {
  final String? name;
  GamePaint({Key? key, this.name}) : super(key: key);

  @override
  _GamePaintState createState() => _GamePaintState();
}

class _GamePaintState extends State<GamePaint> {
  Color selectedColor = Color(0xff443a49);
  Color currentColor = Color(0xff443a49);

  List<dynamic> points = [];
  double opacity = 1.0;
  StrokeCap strokeType = StrokeCap.round;
  double strokeWidth = 1.0;

  // show dialog
  void changeColor(Color color) {
    setState(() => selectedColor = color);
  }

  loadDialog() {
    var lang = AppLocalizations.of(context);

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(
            lang.customColor,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.teal),
          ),
          content: SingleChildScrollView(
            child: ColorPicker(
              pickerColor: selectedColor,
              onColorChanged: changeColor,
              showLabel: true,
              pickerAreaHeightPercent: 0.8,
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.dashboard_customize_outlined),
              onPressed: () {
                setState(() => currentColor = selectedColor);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    if (mounted) setState(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      drawer: DrawerMenu(),
      appBar: AppBar(
        title: Text(
          lang.gamePaint,
        ),
      ),
      floatingActionButton: SpeedDial(
        children: [
          SpeedDialChild(
            child: Icon(
              Icons.radio_button_on_outlined,
              size: 40,
            ),
            foregroundColor: selectedColor,
            backgroundColor: Colors.white,
            label: lang.customColor,
            onTap: () => {
              loadDialog(),
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.radio_button_on_outlined,
              size: 40,
            ),
            foregroundColor: Colors.orange,
            backgroundColor: Colors.white,
            onTap: () => {
              changeColor(Colors.orange),
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.radio_button_on_outlined,
              size: 40,
            ),
            foregroundColor: Colors.blue,
            backgroundColor: Colors.white,
            onTap: () => {
              changeColor(Colors.blue),
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.radio_button_on_outlined,
              size: 40,
            ),
            foregroundColor: Colors.red,
            backgroundColor: Colors.white,
            onTap: () => {
              changeColor(Colors.red),
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.brush,
              size: 40,
            ),
            foregroundColor: selectedColor,
            backgroundColor: Colors.white,
            onTap: () => {
              strokeWidth = 10.0,
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.edit,
              size: 40,
            ),
            foregroundColor: selectedColor,
            backgroundColor: Colors.white,
            onTap: () => {
              strokeWidth = 1.0,
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.clear,
              size: 40,
            ),
            foregroundColor: Colors.black,
            backgroundColor: Colors.white,
            onTap: () => {
              setState(
                () {
                  points.clear();
                },
              )
            },
          ),
        ],
        icon: Icons.color_lens,
        backgroundColor: Colors.green,
      ),
      body: Builder(
        builder: (BuildContext context) {
          return Stack(
            children: [
              GestureDetector(
                onPanUpdate: (details) {
                  setState(() {
                    RenderBox renderBox =
                        context.findRenderObject() as RenderBox;
                    points.add(TouchPoints(
                      points: renderBox.globalToLocal(details.globalPosition),
                      paint: Paint()
                        ..strokeCap = strokeType
                        ..isAntiAlias = true
                        ..color = selectedColor.withOpacity(opacity)
                        ..strokeWidth = strokeWidth,
                    ));
                  });
                },
                onPanStart: (details) {
                  setState(() {
                    RenderBox renderBox =
                        context.findRenderObject() as RenderBox;
                    points.add(TouchPoints(
                      points: renderBox.globalToLocal(details.globalPosition),
                      paint: Paint()
                        ..strokeCap = strokeType
                        ..isAntiAlias = true
                        ..color = selectedColor.withOpacity(opacity)
                        ..strokeWidth = strokeWidth,
                    ));
                  });
                },
                onPanEnd: (details) {
                  setState(() {
                    points.add(null);
                  });
                },
                child: RepaintBoundary(
                  child: Stack(
                    children: [
                      Center(
                        child: Image.asset(
                          Assets.pathImage + widget.name.toString(),
                          fit: BoxFit.fill,
                          // width: 800,
                          // height: 800,
                        ),
                      ),
                      CustomPaint(
                        size: Size.infinite,
                        painter: new MyPainter(
                          pointsList: points,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  List<dynamic> pointsList = [];
  List<Offset> offsetPoints = [];

  MyPainter({required this.pointsList});

  @override
  bool shouldRepaint(MyPainter oldDelegate) => true;

  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < pointsList.length - 1; i++) {
      if (pointsList[i] != null && pointsList[i + 1] != null) {
        canvas.drawLine(pointsList[i].points, pointsList[i + 1].points,
            pointsList[i].paint);
      }

      //  else if (pointsList[i] != null && pointsList[i + 1] == null) {
      //   offsetPoints.clear();
      //   offsetPoints.add(pointsList[i].points);
      //   offsetPoints.add(Offset(
      //       pointsList[i].points.dx + 0.1, pointsList[i].points.dy + 0.1));

      //   //Draw points when two points are not next to each other
      //   canvas.drawPoints(
      //       ui.PointMode.points, offsetPoints, pointsList[i].paint);
      // }
    }
  }
}

class TouchPoints {
  Paint? paint;
  Offset? points;
  TouchPoints({this.points, this.paint});
}
