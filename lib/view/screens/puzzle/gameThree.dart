import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:nhimlearning_app/view/screens/puzzle/gamePaint.dart';
import 'package:nhimlearning_app/view/widgets/card_textbook.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nhimlearning_app/viewmodels/game_three_viewmodel.dart';

class GameThree extends StatefulWidget {
  @override
  State<GameThree> createState() => _GameThreeState();
}

class _GameThreeState extends State<GameThree> {
  final GameThreeViewModel viewModel = GameThreeViewModel();
  late List<dynamic> listItem = [];
  String name = '';
  bool init = false;

  @override
  void didChangeDependencies() {
    loadImages();
    super.didChangeDependencies();
  }

  loadImages() async {
    if (!init) {
      this.name = '';
      init = true;
    }
    await EasyLoading.show(status: "Loading ....");
    listItem = [];
    listItem = await viewModel.getAllImages(0, '');
    if (mounted) setState(() {});
    await EasyLoading.dismiss();
  }

  onTapped(index) {
    // navi to screen draw
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GamePaint(
          name: listItem[index].name.toString(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: Colors.greenAccent,
      drawer: DrawerMenu(),
      body: Builder(
        builder: (BuildContext context) {
          return Stack(
            children: [
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                height: 52,
                                width: 52,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  shape: BoxShape.circle,
                                ),
                                child: IconButton(
                                  onPressed: () =>
                                      Scaffold.of(context).openDrawer(),
                                  icon: Icon(
                                    Icons.menu,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                  lang.gamePaint,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 30,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                height: 52,
                                width: 52,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  shape: BoxShape.circle,
                                ),
                                child: IconButton(
                                  onPressed: () => {
                                    Navigator.of(context)
                                        .popAndPushNamed(Routes.home),
                                  },
                                  icon: Icon(
                                    Icons.home,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: GridView.builder(
                          shrinkWrap: true,
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200,
                            childAspectRatio: 1,
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20,
                          ),
                          itemCount: listItem.length,
                          itemBuilder: (BuildContext ctx, index) {
                            return CardTextBook(
                              isAssetImage: true,
                              title: '',
                              imageLink: listItem[index].name.toString(),
                              isTextToSpeech: false,
                              onTap: () => {onTapped(index)},
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
