import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:nhimlearning_app/constants/color_layout.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';
import 'package:nhimlearning_app/utils/utils.dart';
import 'package:nhimlearning_app/view/widgets/button_gradient.dart';
import 'package:nhimlearning_app/view/widgets/card_textbook.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:nhimlearning_app/viewmodels/animal_cartoon_viewmodel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PuzzleRandomAnimals extends StatefulWidget {
  @override
  State<PuzzleRandomAnimals> createState() => StatePuzzleRandomAnimals();
}

class StatePuzzleRandomAnimals extends State<PuzzleRandomAnimals> {
  final AnimalCartoonViewModel viewModel = AnimalCartoonViewModel();
  late List<dynamic> listItem;
  List<int> listRandomNumber = [];
  int numberRandom = 1;
  int limit = 4;
  int min = 0;
  int max = 4;
  Utils utils = Utils();
  bool init = false;
  bool isNextLevel = false;
  List<int> animalUsed = [];
  int numberAnswer = -1;
  int totalAnimal = 0;
  TextToSpeech tts = TextToSpeech();
  EasyLoading popup = EasyLoading();

  @override
  void initState() {
    autoLoadSpeak();
    getTotalAnimal();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    loadAnimals();
    super.didChangeDependencies();
  }

  autoLoadSpeak() async {
    if (Platform.isAndroid) {
      var lang = AppLocalizations.of(context);
      if (lang.localeName == 'vi') {
        tts.setSpeakVNAndroid();
      } else {
        tts.setSpeakEn();
      }
    }
  }

  getTotalAnimal() async {
    List data = await viewModel.getTotalAnimals();
    if (data.isNotEmpty) {
      totalAnimal = data[0]['total'].toInt();
    }
  }

  loadAnimals() async {
    if (!init) {
      init = true;
      listItem = [];
    }
    await EasyLoading.show(status: "Loading ....");
    listItem = [];
    String except = animalUsed.isEmpty ? "" : animalUsed.join(",").toString();
    listItem = await viewModel.getRandomAnimals(limit, except);
    listRandomNumber = utils.generateListRandomNumber(min, max, limit);
    numberRandom = utils.randomNumber(min, max);
    if (listItem.isNotEmpty) {
      animalUsed.add(listItem[numberRandom].id);
      numberAnswer = listItem[numberRandom].id;
      if (totalAnimal > 0 && animalUsed.length == totalAnimal - limit) {
        animalUsed.clear();
      }
    }
    if (mounted) setState(() {});
    await EasyLoading.dismiss();
  }

  suggestion() {
    var lang = AppLocalizations.of(context);
    if (listItem.isNotEmpty) {
      String text = lang.localeName == 'en'
          ? listItem[numberRandom].nameEn
          : listItem[numberRandom].nameVn;
      tts.text = text;
      tts.speak();
    }
  }

  checkAnswer(int id) async {
    var lang = AppLocalizations.of(context);
    if (id == numberAnswer) {
      tts.text = lang.correct;
      await tts.speak();
      await EasyLoading.showSuccess(lang.correct,
          maskType: EasyLoadingMaskType.black,
          duration: Duration(milliseconds: 500));
      loadAnimals();
    } else {
      tts.text = lang.notCorrect;
      await tts.speak();
      await EasyLoading.showError(lang.notCorrect,
          maskType: EasyLoadingMaskType.black,
          duration: Duration(milliseconds: 500));
    }
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: ColorLayout.kBlueColor,
      drawer: DrawerMenu(),
      appBar: AppBar(
        title: Text(
          lang.puzzle,
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    lang.whatIsThis,
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.all(10),
                  width: 300,
                  height: 300,
                  child: CardTextBook(
                    width: 200,
                    height: 200,
                    title: lang.whatIsThis,
                    isShowTitle: false,
                    imageLink:
                        listItem.isEmpty ? '' : listItem[numberRandom]?.link,
                    isTextToSpeech: false,
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: ButtonGradient(
                                borderRadius: 10,
                                title: listItem.isEmpty
                                    ? ''
                                    : lang.localeName == 'en'
                                        ? listItem[0].nameEn
                                        : listItem[0].nameVn,
                                onPressed: checkAnswer,
                                idButton: listItem.isEmpty ? 0 : listItem[0].id,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: ButtonGradient(
                                borderRadius: 10,
                                title: listItem.isEmpty
                                    ? ''
                                    : lang.localeName == 'en'
                                        ? listItem[1].nameEn
                                        : listItem[1].nameVn,
                                onPressed: checkAnswer,
                                idButton: listItem.isEmpty ? 0 : listItem[1].id,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: ButtonGradient(
                                borderRadius: 10,
                                title: listItem.isEmpty
                                    ? ''
                                    : lang.localeName == 'en'
                                        ? listItem[2].nameEn
                                        : listItem[2].nameVn,
                                onPressed: checkAnswer,
                                idButton: listItem.isEmpty ? 0 : listItem[2].id,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: ButtonGradient(
                                borderRadius: 10,
                                title: listItem.isEmpty
                                    ? ''
                                    : lang.localeName == 'en'
                                        ? listItem[3].nameEn
                                        : listItem[3].nameVn,
                                onPressed: checkAnswer,
                                idButton: listItem.isEmpty ? 0 : listItem[3].id,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () => {
                        suggestion(),
                      },
                      child: Icon(
                        Icons.lightbulb,
                        color: Colors.yellow,
                        size: 70,
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () => {
                        loadAnimals(),
                      },
                      child: Icon(
                        Icons.forward,
                        color: Colors.cyanAccent,
                        size: 70,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
