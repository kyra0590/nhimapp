import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:nhimlearning_app/constants/status.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/models/animals_model.dart';
import 'package:nhimlearning_app/services/api_response.dart';
import 'package:nhimlearning_app/utils/utils.dart';
import 'package:nhimlearning_app/view/widgets/categories_item.dart';
import 'package:nhimlearning_app/view/widgets/categories_rows.dart';
import 'package:nhimlearning_app/view/widgets/drawer_menu.dart';
import 'package:nhimlearning_app/view/widgets/gridview_items.dart';
import 'package:nhimlearning_app/viewmodels/animal_viewmodel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AnimalsPage extends StatefulWidget {
  const AnimalsPage({Key? key}) : super(key: key);

  @override
  _AnimalsPageState createState() => _AnimalsPageState();
}

class _AnimalsPageState extends State<AnimalsPage> {
  bool _init = false;
  List<String> alphas = CStrings.rangeAlphabets;
  AnimalsViewModel viewmodel = AnimalsViewModel();
  int indexAlpha = 0;
  List<Widget> items = [];
  Utils utils = Utils();

  @override
  void didChangeDependencies() {
    _loadAnimals();
    super.didChangeDependencies();
  }

  void onChangeCallBack(value) {
    setState(() {
      indexAlpha = value;
    });
    _loadAnimals();
  }

  void _loadAnimals() async {
    if (!_init) {
      //   // do something
    }
    items = [];
    EasyLoading.show(status: 'Loading...');
    items = await loadItemsByAlphabel(indexAlpha);
    EasyLoading.dismiss();
    _init = true;
    if (mounted) setState(() {});
  }

  Future<List<Widget>> loadItemsByAlphabel(value) async {
    List<Widget> newAnimals = [];
    String alpha = alphas[value].toLowerCase();
    ApiResponse apiResponse = await viewmodel.getAnimalsByAlphabet(alpha);
    if (apiResponse.statusCode == Status.codeSuccess) {
      for (var item in apiResponse.data) {
        var animal = AnimalsModel.fromJson(item);
        newAnimals.add(CategoriesItem(
          title: animal.name,
          urlImage: animal.width_300,
        ));
      }
    } else if (apiResponse.statusCode == Status.codeTimeOut) {
      utils.redirectByTimeOut(context);
    }

    return newAnimals;
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          lang.headerAnimals,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: CategoriesRows(
              indexAlpha: indexAlpha,
              callback: (value) {
                onChangeCallBack(value);
              },
            ),
          ),
          Expanded(
            child: GridViewItems(
              itemOnRow: 2,
              listWidgets: items,
              childAspectRatio: 0.8,
              mainAxisSpacing: 0.5,
              crossAxisSpacing: 0.5,
            ),
          ),
        ],
      ),
      drawer: DrawerMenu(),
    );
  }
}
