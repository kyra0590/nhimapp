import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/sizes.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';

// ignore: camel_case_types
typedef onChangeCallBack = void Function(int val);
List<String> alphas = CStrings.rangeAlphabets;

class CategoriesRows extends StatefulWidget {
  const CategoriesRows({
    Key? key,
    this.indexAlpha,
    required this.callback,
  }) : super(key: key);

  final indexAlpha;
  final onChangeCallBack callback;

  @override
  _CategoriesRowsState createState() => _CategoriesRowsState();
}

class _CategoriesRowsState extends State<CategoriesRows> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 35,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: alphas.length,
        itemBuilder: (context, index) => buildRow(index),
      ),
    );
  }

  Widget buildRow(int index) {
    return GestureDetector(
      onTap: () {
        widget.callback(index);
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              alphas[index],
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: widget.indexAlpha == index
                      ? Colors.black
                      : Colors.black45),
            ),
            Container(
              margin: EdgeInsets.only(top: Sizes.padding20 / 4),
              height: 2,
              width: 30,
              color: widget.indexAlpha == index
                  ? Colors.black
                  : Colors.transparent,
            ),
          ],
        ),
      ),
    );
  }
}
