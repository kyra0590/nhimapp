import 'package:audioplayers/audioplayers.dart';

class AudioWidget {
  AudioCache _audioCache = AudioCache();
  AudioPlayer? _audioPlayer;

  playLocal({path, volumne: 1.0}) async {
    if (path != "")
      _audioPlayer = await _audioCache.play(path, volume: volumne);
  }

  playOnline({url, volumne: 1.0}) async {
    _audioPlayer = AudioPlayer();
    if (url != "") await _audioPlayer!.play(url, volume: volumne);
  }

  stopLocal() {
    _audioPlayer!.stop();
  }
}
