import 'package:flutter/material.dart';

typedef TapCallback = void Function();

// ignore: must_be_immutable
class ItemDrawer extends StatefulWidget {
  IconData icon;
  String text;
  TapCallback onTap;
  bool isEndOfList = false;
  bool isParent = false;
  List<Widget>? listChild;

  ItemDrawer({
    required this.icon,
    required this.text,
    required this.onTap,
    this.isEndOfList = false,
    this.isParent = false,
    this.listChild,
  });

  @override
  _ItemDrawerState createState() => _ItemDrawerState();
}

class _ItemDrawerState extends State<ItemDrawer> {
  @override
  Widget build(BuildContext context) {
    late List<double> paddingEdge =
        widget.isParent == true ? [10, 0, 10, 0] : [10, 0, 10, 0];

    return Container(
      child: InkWell(
        splashColor: Colors.blue,
        onTap: widget.onTap,
        child: Padding(
          padding: EdgeInsets.fromLTRB(
            paddingEdge[0],
            paddingEdge[1],
            paddingEdge[2],
            paddingEdge[3],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              (widget.isParent == false)
                  ? Row(
                      children: [
                        Icon(this.widget.icon),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text(
                            this.widget.text,
                            style: TextStyle(fontSize: 16.0),
                          ),
                        ),
                      ],
                    )
                  : Flexible(
                      flex: 1,
                      child: ExpansionTile(
                        tilePadding: EdgeInsets.all(0),
                        childrenPadding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                        expandedAlignment: Alignment.topLeft,
                        title: Row(
                          children: [
                            Icon(this.widget.icon),
                            Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Text(
                                this.widget.text,
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ],
                        ),
                        children: this.widget.listChild!,
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
