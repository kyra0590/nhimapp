import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/constants/color_layout.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CardTextBook extends StatefulWidget {
  final String title;
  final String desc;
  final String imageLink;
  final bool isTextToSpeech;
  final double width;
  final double height;
  final bool isShowTitle;
  final Function? onTap;
  final bool isAssetImage;

  CardTextBook({
    this.title = '',
    this.desc = '',
    required this.imageLink,
    this.isTextToSpeech = false,
    this.width = 150,
    this.height = 150,
    this.isShowTitle = true,
    this.onTap,
    this.isAssetImage = false,
  });

  @override
  State<CardTextBook> createState() => _CardTextBookState();
}

class _CardTextBookState extends State<CardTextBook> {
  TextToSpeech tts = TextToSpeech();

  @override
  void didChangeDependencies() {
    autoLoadSpeak();
    if (mounted) setState(() {});
    super.didChangeDependencies();
  }

  autoLoadSpeak() async {
    if (Platform.isAndroid) {
      var lang = AppLocalizations.of(context);
      switch (lang.localeName) {
        case 'vi':
          tts.setSpeakVNAndroid();
          break;
        default:
          tts.setSpeakEn();
          break;
      }
    }
  }

  speakText() async {
    tts.text = this.widget.title;
    tts.speak();
  }

  onTapped() {
    if (this.widget.isTextToSpeech) {
      speakText();
    } else {
      Function onta = this.widget.onTap as Function;
      onta();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(13),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(13),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 17),
              blurRadius: 17,
              spreadRadius: -23,
              color: ColorLayout.kShadowColor,
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTapped,
            // widget.isTextToSpeech == true ? speakText() : widget.onTap,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  if (widget.imageLink.isEmpty)
                    Image.asset(
                      Assets.appLogo,
                      width: widget.width,
                      height: widget.height,
                    ),
                  if (widget.isAssetImage && widget.imageLink.isNotEmpty)
                    Image.asset(
                      Assets.pathImage + widget.imageLink,
                      width: widget.width,
                      height: widget.height,
                    ),
                  if (widget.isAssetImage == false &&
                      widget.imageLink.isNotEmpty)
                    Image.network(
                      widget.imageLink,
                      height: widget.height,
                      width: widget.width,
                    ),
                  if (widget.title == "") Spacer(),
                  if (widget.title != "" && widget.isShowTitle == true)
                    Text(
                      widget.title.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown,
                        fontSize: 17,
                      ),
                    ),
                  if (widget.title != "") Spacer(),
                  if (widget.desc != "")
                    Text(
                      (widget.desc != "")
                          ? "\"" + widget.desc.toUpperCase() + "\""
                          : '',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.red,
                      ),
                    ),
                  if (widget.desc != "") Spacer(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
