import 'package:flutter/material.dart';

typedef TapCallback = void Function();

// ignore: must_be_immutable
class ItemDashboard extends StatefulWidget {
  IconData icon;
  String title;
  String subTitle;
  Color color;
  Color colorInk;
  TapCallback tapCallback;

  ItemDashboard(this.icon, this.title, this.subTitle, this.color, this.colorInk,
      this.tapCallback);

  @override
  State<ItemDashboard> createState() => _ItemDashboardState();
}

class _ItemDashboardState extends State<ItemDashboard> {
  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
          color: this.widget.color, borderRadius: BorderRadius.circular(10)),
      child: InkWell(
        splashColor: this.widget.colorInk,
        onTap: this.widget.tapCallback,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                this.widget.icon,
                color: Colors.white,
                size: 50.0,
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 10,
                ),
                child: Text(
                  this.widget.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  this.widget.subTitle,
                  style: TextStyle(
                    color: Colors.white70,
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
