import 'package:flutter/material.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';

class CategoriesItem extends StatefulWidget {
  final title;
  final urlImage;
  final borderColor;
  CategoriesItem(
      {required this.title,
      required this.urlImage,
      this.borderColor = Colors.indigo});

  @override
  State<StatefulWidget> createState() =>
      _CategoriesItemState(this.title, this.urlImage, this.borderColor);
}

class _CategoriesItemState extends State<CategoriesItem> {
  final title;
  final urlImage;
  final borderColor;

  _CategoriesItemState(this.title, this.urlImage, this.borderColor);

  TextToSpeech tts = TextToSpeech();

  @override
  void initState() {
    tts.config();
    super.initState();
  }

  speakText() async {
    tts.text = this.title;
    tts.speak();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {speakText()},
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 5.0),
              decoration: BoxDecoration(
                border: Border.all(color: this.borderColor, width: 2),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Image.network(
                this.urlImage,
                height: 190,
              ),
            ),
            Flexible(
              flex: 1,
              child: Text(
                this.title,
                style: TextStyle(
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
