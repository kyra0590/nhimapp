import 'package:flutter/material.dart';

class GridViewItems extends StatefulWidget {
  final int? itemOnRow;
  final List<Widget> listWidgets;
  final crossAxisSpacing;
  final mainAxisSpacing;
  final childAspectRatio;

  const GridViewItems({
    this.itemOnRow = 2,
    required this.listWidgets,
    required this.crossAxisSpacing,
    this.childAspectRatio = 1.0,
    required this.mainAxisSpacing,
  });

  @override
  GridViewItemsState createState() => GridViewItemsState();
}

class GridViewItemsState extends State<GridViewItems> {
  int? _itemOnRow;
  double? _crossAxisSpacing;
  double? _mainAxisSpacing;
  List<Widget>? _listWidgets;
  double? _childAspectRatio;
  bool _init = false;

  @override
  void initState() {
    _itemOnRow = widget.itemOnRow;
    _childAspectRatio = widget.childAspectRatio;
    _mainAxisSpacing = widget.mainAxisSpacing;
    _crossAxisSpacing = widget.crossAxisSpacing;
    _listWidgets = widget.listWidgets;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_init) {
      _init = true;
      _listWidgets = [];
      if (mounted) setState(() {});
    }
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant GridViewItems oldWidget) {
    if (widget.itemOnRow != oldWidget.itemOnRow ||
        widget.childAspectRatio != oldWidget.childAspectRatio ||
        widget.crossAxisSpacing != oldWidget.crossAxisSpacing ||
        widget.mainAxisSpacing != oldWidget.mainAxisSpacing ||
        widget.listWidgets.length != oldWidget.listWidgets.length) {
      _listWidgets = widget.listWidgets;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      primary: false,
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      childAspectRatio: _childAspectRatio!,
      crossAxisCount: _itemOnRow!,
      crossAxisSpacing: _crossAxisSpacing!,
      mainAxisSpacing: _mainAxisSpacing!,
      children: _listWidgets!,
    );
  }
}
