import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ButtonGradient extends StatefulWidget {
  List<dynamic>? listColors = [
    Colors.lightBlue,
    Colors.blue,
    Colors.blueAccent,
  ];
  final double borderRadius;
  final String title;
  final Function(int idButton)? onPressed;
  final int idButton;

  ButtonGradient({
    this.idButton = 0,
    required this.title,
    required this.borderRadius,
    this.listColors,
    this.onPressed,
  });

  @override
  _StateButtonGradient createState() => _StateButtonGradient();
}

class _StateButtonGradient extends State<ButtonGradient> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(widget.borderRadius),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.lightBlue,
              Colors.blue,
              Colors.blueAccent,
            ],
          ),
        ),
        child: TextButton(
          style: TextButton.styleFrom(
            primary: Colors.white,
            textStyle: TextStyle(fontSize: 14),
          ),
          onPressed: () {
            var onPressed = widget.onPressed!;
            onPressed(widget.idButton.toInt());
          },
          child: Text(
            widget.title.toUpperCase(),
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
