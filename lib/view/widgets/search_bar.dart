import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchBar extends StatelessWidget {
  final String name;
  final Function onChange;

  SearchBar({required this.name, required this.onChange});

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(29.5),
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: lang.search,
          icon: Icon(Icons.search),
          border: InputBorder.none,
        ),
        onChanged: (text) => {
          onChange(text),
        },
      ),
    );
  }
}
