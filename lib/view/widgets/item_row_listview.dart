import 'package:flutter/material.dart';
import 'package:nhimlearning_app/utils/text_to_speech.dart';

class ItemRowListView extends StatefulWidget {
  final title;
  final urlImage;
  final borderColor;
  ItemRowListView(
      {required this.title,
      required this.urlImage,
      this.borderColor = Colors.indigo});

  @override
  State<StatefulWidget> createState() =>
      _ItemRowListViewState(this.title, this.urlImage, this.borderColor);
}

class _ItemRowListViewState extends State<ItemRowListView> {
  final title;
  final urlImage;
  final borderColor;

  _ItemRowListViewState(this.title, this.urlImage, this.borderColor);

  TextToSpeech tts = TextToSpeech();

  @override
  void initState() {
    tts.config();
    super.initState();
  }

  speakText() {
    tts.text = this.title;
    tts.speak();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {speakText()},
      child: Container(
        padding: EdgeInsets.all(5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: EdgeInsets.all(0),
              margin: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                border: Border.all(color: this.borderColor, width: 2),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Image.network(
                this.urlImage,
                height: 150,
              ),
            ),
            Text(
              this.title,
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
