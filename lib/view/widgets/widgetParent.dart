import 'package:flutter/material.dart';

class WidgetParent extends StatefulWidget {
  @override
  State<WidgetParent> createState() => _WidgetParentState();
}

class _WidgetParentState extends State<WidgetParent> {
  int count = 0;

  onIncrease() {
    setState(() {
      this.count += 1;
    });
    print(count.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test parent and child'),
      ),
      body: SafeArea(
        child: Container(
          child: Center(
            child: Column(
              children: [
                Text(
                  "Count:" + count.toString(),
                  style: TextStyle(fontSize: 30),
                ),
                WidgetChild(
                  onParentChange: onIncrease,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class WidgetChild extends StatelessWidget {
  final Function onParentChange;

  WidgetChild({required this.onParentChange});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: IconButton(
        iconSize: 50,
        icon: Icon(Icons.cloud_circle),
        onPressed: () {
          this.onParentChange();
          print('press');
        },
      ),
    );
  }
}
