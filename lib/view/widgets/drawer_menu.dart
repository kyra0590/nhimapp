import 'package:flutter/material.dart';
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/constants/color_layout.dart';
import 'package:nhimlearning_app/constants/menus.dart';
import 'package:nhimlearning_app/models/sharedpref/shared_preference_helper.dart';
import 'package:nhimlearning_app/services/service_locator.dart';
import 'package:nhimlearning_app/view/widgets/item_drawer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DrawerMenu extends StatefulWidget {
  @override
  State<DrawerMenu> createState() => _DrawerMenuState();
}

class _DrawerMenuState extends State<DrawerMenu> {
  SharedPreferenceHelper _sharedPrefsHelper = locator<SharedPreferenceHelper>();
  bool isLocal = false;
  late List<dynamic> menuItems = [];

  @override
  void initState() {
    checkIsLocal();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    checkIsLocal();
    super.didChangeDependencies();
  }

  checkIsLocal() async {
    isLocal = await _sharedPrefsHelper.isLocal;
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations lang = AppLocalizations.of(context);

    List<Widget> buidlItems(bool local) {
      Menus menus = Menus(lang: lang);

      List<dynamic> menuItems =
          local == true ? menus.loadNaviLocal() : menus.loadNaviOnline();

      List<Widget> listWidget = [];

      for (var itemParent in menuItems) {
        if (itemParent['children'] == null) {
          listWidget.add(
            ItemDrawer(
              icon: itemParent['icon'],
              text: itemParent['title'],
              onTap: () =>
                  {Navigator.of(context).popAndPushNamed(itemParent['link'])},
              isParent: false,
            ),
          );
        } else {
          List<Widget> listChild = [];
          // ignore: unused_local_variable
          for (var itemChild in itemParent['children']) {
            listChild.add(
              ItemDrawer(
                icon: itemChild['icon'],
                text: itemChild['title'],
                onTap: () =>
                    {Navigator.of(context).popAndPushNamed(itemChild['link'])},
                isParent: false,
              ),
            );
          }

          listWidget.add(
            ItemDrawer(
              icon: itemParent['icon'],
              text: itemParent['title'],
              onTap: () =>
                  {Navigator.of(context).popAndPushNamed(itemParent['link'])},
              isParent: true,
              listChild: listChild,
            ),
          );
        }
      }

      return listWidget;
    }

    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 250,
            child: DrawerHeader(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: [
                        0.1,
                        0.4,
                        0.6,
                        0.9,
                      ],
                      colors: ColorLayout.backgroundHeaderDrawer)),
              child: Container(
                child: Column(
                  children: [
                    Text(
                      lang.appName,
                      style: TextStyle(
                        color: ColorLayout.headerDrawer,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 15.0, 0, 0),
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(Assets.appLogo),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                children: [
                  ...buidlItems(this.isLocal),
                  // next future
                  // ItemDrawer(
                  //   icon: Icons.logout,
                  //   text: lang.logout,
                  //   onTap: () => {
                  //     utils.redirectByTimeOut(context),
                  //   },
                  //   isParent: false,
                  // ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
