
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastWidget {

  Toast? toastLength;
  int timeInSecForIosWeb = 1;
  double? fontSize;
  ToastGravity? gravity;
  Color? backgroundColor;
  Color? textColor;
  bool webShowClose = false;
  String? webBgColor= "linear-gradient(to right, #00b09b, #96c93d)";
  String? webPosition= "right";
  String? msg;

  ToastWidget() {
    this.timeInSecForIosWeb = 2;
    this.fontSize = 15;
    // this.backgroundColor = Colors.redAccent;
    // this.textColor = Colors.white;
  }

  showMessage(String msg) {
    this.backgroundColor = Colors.black;
    this.textColor = Colors.white;
    showToast(msg);
  }

  successMessage(String msg){
    this.backgroundColor = Colors.green;
    this.textColor = Colors.white;
    showToast(msg);
  }

  errorMessage(String msg){
    this.backgroundColor = Colors.redAccent;
    this.textColor = Colors.white;
    showToast(msg);
  }

  showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: toastLength,
      timeInSecForIosWeb: timeInSecForIosWeb,
      fontSize: fontSize,
      gravity: gravity,
      backgroundColor: backgroundColor,
      textColor: textColor,
      webShowClose: webShowClose,
      webBgColor: webBgColor,
      webPosition: webPosition
    );
  }

}
