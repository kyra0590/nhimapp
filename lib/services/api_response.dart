import 'package:nhimlearning_app/constants/status.dart';

class ApiResponse {
  int status;
  String? message;
  dynamic data;
  int statusCode;

  ApiResponse({
    this.status = Status.initialData,
    this.data,
    this.message,
    this.statusCode = 0,
  });

  loading() {
    this.status = Status.loadingData;
  }

  completed(dynamic result, int statusCode) {
    this.status = Status.completedData;
    if (result['data'] != null) {
      this.data = result['data'];
    } else {
      this.data = result;
    }
    this.statusCode = statusCode;
  }

  error(String message, int statusCode) {
    this.status = Status.errorData;
    this.message = message;
    this.statusCode = statusCode;
  }
}
