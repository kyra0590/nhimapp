import 'package:get_it/get_it.dart';
import 'package:nhimlearning_app/models/sharedpref/shared_preference_helper.dart';

final locator = GetIt.instance;

Future<void> setupLocator() async {
  locator.registerSingleton<SharedPreferenceHelper>(
      await SharedPreferenceHelper.getInstance());
}
