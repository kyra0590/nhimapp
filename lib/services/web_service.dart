import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:nhimlearning_app/services/api_response.dart';

class WebService {
  final String baseUrl = "http://localhost/api";

  var dio = new Dio();

  Future postResponse(String endPoint, FormData formData, String token) async {
    String url = Uri.parse(baseUrl + endPoint).toString();
    dynamic responseJson;
    try {
      if (token != '') {
        dio.options.headers["Authorization"] = "Bearer " + token;
      }
      final response = await dio.post(
        url,
        data: formData,
      );
      responseJson = returnResponse(response);
    } on DioError catch (e) {
      responseJson = returnResponse(e.response);
    }
    return responseJson;
  }

  Future getResponse(
      String endPoint, dynamic queryParameters, String token) async {
    String url = Uri.parse(baseUrl + endPoint).toString();
    dynamic apiResponse;
    try {
      if (token != '') {
        dio.options.headers["Authorization"] = "Bearer " + token;
      }
      final response = await dio.get(
        url,
        queryParameters: queryParameters,
      );
      apiResponse = returnResponse(response);
    } on DioError catch (e) {
      apiResponse = returnResponse(e.response);
    }
    return apiResponse;
  }

  @visibleForTesting
  ApiResponse returnResponse(dynamic response) {
    ApiResponse apiResponse = ApiResponse();
    apiResponse.statusCode = response.statusCode;
    switch (response.statusCode) {
      case 200:
      case 201:
        apiResponse.data = response.data;
        break;
      case 400:
      case 401:
      case 403:
      case 405:
        apiResponse.message = response.data['message'].toString();
        break;
      case 500:
      default:
        apiResponse.message = "SERVER ERROR !!!";
        break;
    }
    return apiResponse;
  }
}
