import 'dart:io' as io;
import 'package:nhimlearning_app/constants/assets.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:flutter/services.dart' show rootBundle;

class LocalDatabase {
  static final LocalDatabase instance = LocalDatabase._init();

  Database? _database;

  LocalDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDb(CStrings.dbName);
    return _database!;
  }

  Future<Database> initDb(String fileName) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, fileName);
    bool existDb = await instance.exitsData(CStrings.dbName);
    Database db = await openDatabase(path, version: 1);
    if (existDb == false) {
      await importData(db, '');
    }
    return db;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  Future<bool> exitsData(String dbName) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, dbName);
    print(path);
    return io.File(path).exists();
  }

  Future<void> importData(Database? db, String sql) async {
    if (db == null) {
      db = await instance.database;
    }
    if (sql != '') {
      db.execute(sql);
    }
    var listFiles = [
      "animals",
      "dataAnimals",
      "animalsCartoon",
      "dataAnimalsCartoon",
      "gameTwo",
      "dataGameTwo",
      "gameThree",
      "dataGameThree",
    ];
    for (var file in listFiles) {
      String sqlData = await loadSchemas(Assets.schemasData + file + '.sql');
      await db.execute(sqlData);
    }
  }

  Future<String> loadSchemas(String path) async {
    return await rootBundle.loadString(path);
  }

  Future<dynamic> getAllData(String sql) async {
    final db = await instance.database;
    var result = await db.rawQuery(sql);
    return result.toList();
  }

  Future<dynamic> getTotalData(String sql) async {
    final db = await instance.database;
    var result = await db.rawQuery(sql);
    return result;
  }
}
