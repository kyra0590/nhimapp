class Status {
  Status._();
  static const int initialData = 0;
  static const int loadingData = 1;
  static const int completedData = 2;
  static const int errorData = 3;

  static const int codeSuccess = 200;
  static const int codeCreated = 201;
  static const int codeBadRequest = 400;
  static const int codeNotFound = 404;
  static const int codeServerError = 500;
  static const int codeTimeOut = 401;
}
