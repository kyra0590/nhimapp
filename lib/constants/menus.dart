import 'package:flutter/material.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Menus {
  final AppLocalizations lang;

  Menus({required this.lang});

  loadNaviLocal() {
    return [
      {
        'title': lang.headerHome,
        'link': Routes.home,
        'icon': Icons.home,
      },
      {
        'title': lang.learnEnglish,
        'link': '',
        'icon': Icons.language,
        'children': [
          {
            'title': lang.headerAnimals,
            'link': Routes.animalsLocal,
            'icon': Icons.android,
          },
          {
            'title': lang.headerAnimalsCartoon,
            'link': Routes.animalsCartoon,
            'icon': Icons.card_travel,
          },
          {
            'title': lang.headerAlphabets,
            'link': Routes.alphabets,
            'icon': Icons.sort_by_alpha,
          },
          {
            'title': lang.headerNumbers,
            'link': Routes.numbers,
            'icon': Icons.pin,
          },
          {
            'title': lang.colors,
            'link': Routes.learnColors,
            'icon': Icons.grade,
          }
          // {
          //   'title': lang.hearderSttOne,
          //   'link': Routes.learnSttOne,
          //   'icon': Icons.record_voice_over_outlined,
          // }
        ],
      },
      {
        'title': lang.puzzle,
        'link': '',
        'icon': Icons.gamepad,
        'children': [
          {
            'title': lang.randomAnimal,
            'link': Routes.puzzleRandomAnimals,
            'icon': Icons.games_outlined,
          },
          {
            'title': lang.gameTwo,
            'link': Routes.gameTwo,
            'icon': Icons.games_outlined,
          },
          {
            'title': lang.gamePaint,
            'link': Routes.gameThree,
            'icon': Icons.photo,
          },
        ],
      },
      {
        'title': lang.headerStories,
        'link': Routes.genreOfStories,
        'icon': Icons.book,
      },
      {
        'title': lang.headerSettings,
        'link': Routes.settings,
        'icon': Icons.settings,
      },
    ];
  }

  loadNaviOnline() {
    return [
      {
        'title': lang.headerHome,
        'link': Routes.home,
        'icon': Icons.home,
      },
      {
        'title': lang.learnEnglish,
        'link': '',
        'icon': Icons.language,
        'children': [
          {
            'title': lang.headerAnimals,
            'link': Routes.animals,
            'icon': Icons.android,
          },
          {
            'title': lang.headerAnimalsCartoon,
            'link': Routes.animalsCartoon,
            'icon': Icons.card_travel,
          },
          {
            'title': lang.headerAlphabets,
            'link': Routes.alphabets,
            'icon': Icons.sort_by_alpha,
          },
          {
            'title': lang.headerNumbers,
            'link': Routes.numbers,
            'icon': Icons.pin,
          },
          {
            'title': lang.colors,
            'link': Routes.learnColors,
            'icon': Icons.grade,
          },
          {
            'title': lang.hearderSttOne,
            'link': Routes.learnSttOne,
            'icon': Icons.record_voice_over_outlined,
          }
        ],
      },
      {
        'title': lang.puzzle,
        'link': '',
        'icon': Icons.gamepad,
        'children': [
          {
            'title': lang.randomAnimal,
            'link': Routes.puzzleRandomAnimals,
            'icon': Icons.games_outlined,
          },
          {
            'title': lang.gameTwo,
            'link': Routes.gameTwo,
            'icon': Icons.games_outlined,
          },
        ],
      },
      {
        'title': lang.headerSettings,
        'link': Routes.settings,
        'icon': Icons.settings,
      },
    ];
  }

  loadMenuLocal() {
    return [
      {
        'title': lang.headerAnimals,
        'link': Routes.animalsLocal,
        'icon': Icons.android,
      },
      {
        'title': lang.headerAnimalsCartoon,
        'link': Routes.animalsCartoon,
        'icon': Icons.card_travel,
      },
      {
        'title': lang.headerAlphabets,
        'link': Routes.alphabets,
        'icon': Icons.sort_by_alpha,
      },
      {
        'title': lang.headerNumbers,
        'link': Routes.numbers,
        'icon': Icons.pin,
      },
      {
        'title': lang.randomAnimal,
        'link': Routes.puzzleRandomAnimals,
        'icon': Icons.games_outlined,
      },
      {
        'title': lang.colors,
        'link': Routes.learnColors,
        'icon': Icons.grade,
      },
      {
        'title': lang.gameTwo,
        'link': Routes.gameTwo,
        'icon': Icons.games_outlined,
      },
      {
        'title': lang.gamePaint,
        'link': Routes.gameThree,
        'icon': Icons.photo,
      },
      {
        'title': lang.headerStories,
        'link': Routes.genreOfStories,
        'icon': Icons.book,
      },
      // {
      //   'title': lang.hearderSttOne,
      //   'link': Routes.learnSttOne,
      //   'icon': Icons.record_voice_over_outlined,
      // }
    ];
  }

  loadMenuOnline() {
    return [
      {
        'title': lang.headerAnimals,
        'link': Routes.animals,
        'icon': Icons.android,
      },
      {
        'title': lang.headerAnimalsCartoon,
        'link': Routes.animalsCartoon,
        'icon': Icons.card_travel,
      },
      {
        'title': lang.headerAlphabets,
        'link': Routes.alphabets,
        'icon': Icons.sort_by_alpha,
      },
      {
        'title': lang.headerNumbers,
        'link': Routes.numbers,
        'icon': Icons.pin,
      },
      {
        'title': lang.randomAnimal,
        'link': Routes.puzzleRandomAnimals,
        'icon': Icons.games_outlined,
      },
      {
        'title': lang.colors,
        'link': Routes.learnColors,
        'icon': Icons.grade,
      },
      {
        'title': lang.gameTwo,
        'link': Routes.gameTwo,
        'icon': Icons.games_outlined,
      },
      // {
      //   'title': lang.hearderSttOne,
      //   'link': Routes.learnSttOne,
      //   'icon': Icons.record_voice_over_outlined,
      // }
    ];
  }
}
