class CStrings {
  CStrings._();
  static const String appName = "Nhim Learning";
  static const String headerTextBooks = 'TextBooks';
  static const int limitRangeNumber = 101;
  static const List<String> rangeAlphabets = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z'
  ];
  static String dbName = 'local_database.db';
  static String apiUrl = 'http://localhost/';
  static String apiImageUrl = 'http://localhost/public/images/';
  static const List<String> languages = ['en', 'vi'];
  static const language = "language";
  static const dynamic listColors = [
    {"color": 0xfff44336, "en": "Red", "vi": "Đỏ"},
    {"color": 0xffffffff, "en": "white", "vi": "Trắng"},
    {"color": 0xffffeb3b, "en": "yellow", "vi": "Vàng"},
    {"color": 0xff000000, "en": "black", "vi": "Đen"},
    {"color": 0xff2196f3, "en": "blue", "vi": "Xanh dương"},
    {"color": 0xff795548, "en": "brown", "vi": "Nâu"},
    {"color": 0xff00bcd4, "en": "cyan", "vi": "Lam"},
    {"color": 0xff4caf50, "en": "green", "vi": "Xanh lá cây"},
    {"color": 0xff9e9e9e, "en": "grey", "vi": "Bạc"},
    {"color": 0xff3f51b5, "en": "indigo", "vi": "Chàm"},
    {"color": 0xff03a9f4, "en": "light blue", "vi": "Xanh nước biển"},
    {"color": 0xffcddc39, "en": "lime", "vi": "vàng chanh"},
    {"color": 0xffff9800, "en": "orange", "vi": "Cam"},
    {"color": 0xffe91e63, "en": "pink", "vi": "Hồng"},
    {"color": 0xff9c27b0, "en": "purple", "vi": "Tím"},
    {"color": 0xff009688, "en": "teal", "vi": "Xanh lục"},
  ];

  static const List game2Map = ["😆", "👋🏼", "🧶", "☂️", "💍", "🧤"];
}
