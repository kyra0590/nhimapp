import 'package:flutter/material.dart';

class ColorLayout {
  ColorLayout._();

  // Background header drawer
  static const List<Color> backgroundHeaderDrawer = <Color>[
    Colors.yellow,
    Colors.red,
    Colors.indigo,
    Colors.teal,
  ];

  static const Color headerDrawer = Colors.white;

  static const bgAnimalCartoon = Color(0xFFC7B8F5);
  static const kActiveIconColor = Color(0xFFE68342);
  static const kTextColor = Color(0xFF222B45);
  static const kBlueLightColor = Color(0xFFC7B8F5);
  static const kBlueColor = Color(0xFF817DC0);
  static const kShadowColor = Color(0xFFE6E6E6);
}
