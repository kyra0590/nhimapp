class Assets {
  Assets._();

  // splash screen assets
  static const String appLogo = "assets/images/logo.png";

  static const String bgLeaf = "assets/images/bgLeaf.png";

  // login screen assets
  static const String loginBackground = "assets/images/background.png";
  static const String loginLight_1 = "assets/images/light-1.png";
  static const String loginLight_2 = "assets/images/light-2.png";
  static const String loginClock = "assets/images/clock.png";
  static const String logoNoColor = "assets/images/logo_nocolor.png";
  static const String schemasData = "assets/schemas/";
  static const String pathImage = "assets/images/";
}
