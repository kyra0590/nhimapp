class Sounds {
  Sounds._();

  static const String correct = "sounds/correct.mp3";
  static const String wrong = "sounds/wrong.mp3";
  static const String crabdance = "sounds/crab_dance.mp3";
}
