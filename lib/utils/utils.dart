import 'dart:math';

import 'package:flutter/material.dart';
import 'package:nhimlearning_app/models/sharedpref/constants/preferences.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils {
  Utils();
  Utils.redirect(dynamic context, String routeName) {
    Navigator.of(context).popAndPushNamed(routeName);
  }

  Future<void> redirectByTimeOut(dynamic context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(Preferences.auth_token, '');
    preferences.setBool(Preferences.is_logged_in, false);
    preferences.setBool(Preferences.is_local, false);
    Navigator.of(context).popAndPushNamed(Routes.login);
  }

  int randomNumber(int min, int max) {
    var random = new Random();
    int result = min + random.nextInt(max - min);
    return result;
  }

  List<int> generateListRandomNumber(int min, int max, int limit) {
    List<int> listNumber = [];
    int count = 0;
    if (limit > 0 && min < max) {
      while (count < limit) {
        int numberRandom = randomNumber(min, max);
        if (!listNumber.contains(numberRandom)) {
          listNumber.add(numberRandom);
          count++;
        }
      }
    }
    return listNumber;
  }
}
