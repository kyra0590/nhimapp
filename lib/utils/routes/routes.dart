import 'package:flutter/material.dart';
import 'package:nhimlearning_app/view/screens/alphabets.dart';
import 'package:nhimlearning_app/view/screens/animal_cartoon.dart';
import 'package:nhimlearning_app/view/screens/animals.dart';
import 'package:nhimlearning_app/view/screens/animals_local.dart';
import 'package:nhimlearning_app/view/screens/book/genreOfStories.dart';
import 'package:nhimlearning_app/view/screens/home.dart';
import 'package:nhimlearning_app/view/screens/learn_colors.dart';
import 'package:nhimlearning_app/view/screens/learn_numbers.dart';
import 'package:nhimlearning_app/view/screens/login.dart';
import 'package:nhimlearning_app/view/screens/puzzle/gamePaint.dart';
import 'package:nhimlearning_app/view/screens/puzzle/gameThree.dart';
import 'package:nhimlearning_app/view/screens/puzzle/gameTwo.dart';
import 'package:nhimlearning_app/view/screens/puzzle/randomAnimals.dart';
import 'package:nhimlearning_app/view/screens/settings.dart';
import 'package:nhimlearning_app/view/screens/splash.dart';
import 'package:nhimlearning_app/view/screens/sttext/learn_stt_one.dart';
import 'package:nhimlearning_app/view/screens/textbooks.dart';

class Routes {
  Routes._();

  //static variables
  static const String home = '/';
  static const String login = '/login';
  static const String intro = '/intro';
  static const String animals = '/animals';
  static const String animalsLocal = '/animalsLocal';
  static const String alphabets = '/alphabets';
  static const String numbers = '/numbers';
  static const String textbooks = '/textbooks';
  static const String animalsCartoon = '/animalCartoon';
  static const String settings = '/settings';
  static const String puzzleRandomAnimals = "/puzzleRandomAnimals";
  static const String learnColors = '/colors';
  static const String gameTwo = '/gameTwo';
  static const String testIcons = '/testIcons';
  static const String gamePaint = '/gamePaint';
  static const String gameThree = '/gameThree';
  static const String learnSttOne = '/learnSttOne';
  static const String genreOfStories = '/genreOfStories';

  static final routes = <String, WidgetBuilder>{
    login: (BuildContext context) => LoginPage(),
    home: (BuildContext context) => HomePage(),
    intro: (BuildContext context) => SplashPage(),
    animals: (BuildContext context) => AnimalsPage(),
    animalsLocal: (BuildContext context) => AnimalsLocalPage(),
    alphabets: (BuildContext context) => AlphabetsPage(),
    numbers: (BuildContext context) => LearnNumbersPage(),
    textbooks: (BuildContext context) => TextBooksPage(),
    animalsCartoon: (BuildContext context) => AnimalCartoonPage(),
    settings: (BuildContext context) => SettingsPage(),
    puzzleRandomAnimals: (BuildContext context) => PuzzleRandomAnimals(),
    learnColors: (BuildContext context) => LearnColors(),
    gameTwo: (BuildContext context) => GameTwo(),
    gamePaint: (BuildContext context) => GamePaint(),
    gameThree: (BuildContext context) => GameThree(),
    learnSttOne: (BuildContext context) => LearnSttOne(),
    genreOfStories: (BuildContext context) => GenreOfStories(),
  };
}
