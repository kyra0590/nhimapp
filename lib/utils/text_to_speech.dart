import 'dart:io';

import 'package:flutter_tts/flutter_tts.dart';

class TextToSpeech {
  String text = '';
  FlutterTts flutterTts = FlutterTts();
  // IOS
  bool sharedAudio = true;
  bool setAudioBluetooth = true;
  bool speakCompletion = true;
  bool awaitSynthCompletion = true;

  String language = 'en-US';
  double speechRate = 1.0;
  double volumne = 1.0;
  double pitch = 1.0;
  Map<String, String> voice = {"name": "Karen", "locale": "en-AU"};

  Future config() async {
    if (Platform.isIOS) {
      await flutterTts.setSharedInstance(sharedAudio);
      if (setAudioBluetooth) {
        await flutterTts
            .setIosAudioCategory(IosTextToSpeechAudioCategory.playAndRecord, [
          IosTextToSpeechAudioCategoryOptions.allowBluetooth,
          IosTextToSpeechAudioCategoryOptions.allowBluetoothA2DP,
          IosTextToSpeechAudioCategoryOptions.mixWithOthers
        ]);
      }
    }

    await flutterTts.awaitSpeakCompletion(speakCompletion);
    // await flutterTts.awaitSynthCompletion(awaitSynthCompletion);

    await flutterTts.setLanguage(language);
    // await flutterTts.setSpeechRate(speechRate);
    await flutterTts.setVolume(volumne);
    // await flutterTts.setPitch(pitch);
    // await flutterTts.isLanguageAvailable("en-US");
    await flutterTts.setVoice(voice);
  }

  Future speak() async {
    if (text != '') {
      await flutterTts.speak(this.text);
    }
  }

  Future stop() async {
    await flutterTts.stop();
  }

  Future getLanguages() async {
    return await flutterTts.getLanguages;
  }

  Future getVoices() async {
    return await flutterTts.getVoices;
  }

  Future setSpeakVNAndroid() async {
    if (Platform.isAndroid) {
      await flutterTts.setLanguage('vi-VN');
      await flutterTts
          .setVoice({"name": "vi-vn-x-vie-local", "locale": "vi-VN"});
    }
  }

  Future setSpeakEn() async {
    await flutterTts.setLanguage('en-US');
    await flutterTts.setVoice({"name": "Karen", "locale": "en-AU"});
  }
}
