import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:nhimlearning_app/constants/status.dart';
import 'package:nhimlearning_app/models/login_model.dart';
import 'package:nhimlearning_app/models/sharedpref/shared_preference_helper.dart';
import 'package:nhimlearning_app/services/api_response.dart';
import 'package:nhimlearning_app/services/service_locator.dart';
import 'package:nhimlearning_app/viewmodels/login_viewmodel.dart';
import 'package:validators/validators.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
part 'login_mbx.g.dart';

class LoginMbx = _LoginMbx with _$LoginMbx;

abstract class _LoginMbx with Store {
  final LoginErrorState error = LoginErrorState();
  final LoginViewModel viewmodel = new LoginViewModel();
  final SharedPreferenceHelper _sharedPrefsHelper =
      locator<SharedPreferenceHelper>();

  AppLocalizations? locale;

  @observable
  String lang = 'en';

  @observable
  String email = '';

  @observable
  String password = '';

  _LoginMbx() {
    loadLocale(lang);
  }

  @action
  void setLang(value) {
    lang = value;
    loadLocale(value);
  }

  @action
  void setEmail(value) {
    email = value;
  }

  @action
  void setPassword(value) {
    password = value;
  }

  loadLocale(value) async {
    locale = await AppLocalizations.delegate.load(Locale(value));
  }

  @computed
  bool get canLogin => !error.hasErrors;

  late List<ReactionDisposer> _disposers;

  void setupValidations() {
    _disposers = [
      reaction((_) => email, validateEmail),
      reaction((_) => password, validatePassword)
    ];
  }

  @action
  void validatePassword(String value) {
    error.password =
        isNull(value) || value.isEmpty ? locale!.errorIsBlank : null;
  }

  @action
  void validateEmail(String value) {
    error.email = isEmail(value) ? null : locale!.errorValidEmail;
  }

  void dispose() {
    for (final d in _disposers) {
      d();
    }
  }

  void validateAll() {
    validatePassword(password);
    validateEmail(email);
  }

  Future<void> executeLogin() async {
    validateAll();
    if (canLogin) {
      if (email == 'nhimcon@gmail.com' && password == 'nhimconbebong') {
        await _sharedPrefsHelper.saveAuthToken("");
        await _sharedPrefsHelper.saveIsLocal(true);
        await _sharedPrefsHelper.saveIsLoggedIn(true);
      } else {
        ApiResponse response = await viewmodel.fetchLogin(email, password);
        if (response.statusCode == Status.codeSuccess) {
          var data = LoginModel.fromJson(response.data);
          await _sharedPrefsHelper.saveAuthToken(data.accessToken.toString());
          await _sharedPrefsHelper.saveIsLoggedIn(true);
        } else {
          error.email = locale!.messageCheckEmailPass;
        }
        await _sharedPrefsHelper.saveIsLocal(false);
      }
    }
  }
}

class LoginErrorState = _LoginErrorState with _$LoginErrorState;

abstract class _LoginErrorState with Store {
  @observable
  String? email;

  @observable
  String? password;

  @computed
  bool get hasErrors => email != null || password != null;
}
