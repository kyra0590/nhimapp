// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_mbx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginMbx on _LoginMbx, Store {
  Computed<bool>? _$canLoginComputed;

  @override
  bool get canLogin => (_$canLoginComputed ??=
          Computed<bool>(() => super.canLogin, name: '_LoginMbx.canLogin'))
      .value;

  final _$langAtom = Atom(name: '_LoginMbx.lang');

  @override
  String get lang {
    _$langAtom.reportRead();
    return super.lang;
  }

  @override
  set lang(String value) {
    _$langAtom.reportWrite(value, super.lang, () {
      super.lang = value;
    });
  }

  final _$emailAtom = Atom(name: '_LoginMbx.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_LoginMbx.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$_LoginMbxActionController = ActionController(name: '_LoginMbx');

  @override
  void setLang(dynamic value) {
    final _$actionInfo =
        _$_LoginMbxActionController.startAction(name: '_LoginMbx.setLang');
    try {
      return super.setLang(value);
    } finally {
      _$_LoginMbxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEmail(dynamic value) {
    final _$actionInfo =
        _$_LoginMbxActionController.startAction(name: '_LoginMbx.setEmail');
    try {
      return super.setEmail(value);
    } finally {
      _$_LoginMbxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPassword(dynamic value) {
    final _$actionInfo =
        _$_LoginMbxActionController.startAction(name: '_LoginMbx.setPassword');
    try {
      return super.setPassword(value);
    } finally {
      _$_LoginMbxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validatePassword(String value) {
    final _$actionInfo = _$_LoginMbxActionController.startAction(
        name: '_LoginMbx.validatePassword');
    try {
      return super.validatePassword(value);
    } finally {
      _$_LoginMbxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateEmail(String value) {
    final _$actionInfo = _$_LoginMbxActionController.startAction(
        name: '_LoginMbx.validateEmail');
    try {
      return super.validateEmail(value);
    } finally {
      _$_LoginMbxActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
lang: ${lang},
email: ${email},
password: ${password},
canLogin: ${canLogin}
    ''';
  }
}

mixin _$LoginErrorState on _LoginErrorState, Store {
  Computed<bool>? _$hasErrorsComputed;

  @override
  bool get hasErrors =>
      (_$hasErrorsComputed ??= Computed<bool>(() => super.hasErrors,
              name: '_LoginErrorState.hasErrors'))
          .value;

  final _$emailAtom = Atom(name: '_LoginErrorState.email');

  @override
  String? get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String? value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_LoginErrorState.password');

  @override
  String? get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String? value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  @override
  String toString() {
    return '''
email: ${email},
password: ${password},
hasErrors: ${hasErrors}
    ''';
  }
}
