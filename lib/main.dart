import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:nhimlearning_app/constants/cstrings.dart';
import 'package:nhimlearning_app/provider/locale_provider.dart';
import 'package:nhimlearning_app/services/local_database.dart';
import 'package:nhimlearning_app/services/service_locator.dart';
import 'package:nhimlearning_app/utils/routes/routes.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => LocaleProvider(),
        builder: (context, child) {
          final provider = Provider.of<LocaleProvider>(context);
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            initialRoute: Routes.intro,
            routes: Routes.routes,
            theme: ThemeData(
              primaryColor: Colors.blueAccent,
              fontFamily: "Cairo",
            ),
            builder: EasyLoading.init(),
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            locale: provider.locale,
          );
        },
      );
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  LocalDatabase localDatabase = LocalDatabase.instance;
  if (await localDatabase.exitsData(CStrings.dbName) == false) {
    await localDatabase.initDb(CStrings.dbName);
  }
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  return runZonedGuarded(() async {
    runApp(MyApp());
  }, (error, stack) {
    print(stack);
    print(error);
  });
}
