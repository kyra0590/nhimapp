CREATE TABLE animals (
    id int,
    name string,
    alpha string,
    width_150 string,
    width_300 string,
    width_500 string,
    created_at string,
    updated_at string,
    PRIMARY KEY (id));